import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
/*import App from './App';*/
/*import * as serviceWorker from './serviceWorker';*/

class Taker extends React.Component{

  /*constructor(props){
    super(props);
  }*/

  render(){
    return(
      /*<div className= 'taker'>
       <p>{this.props.value[0]}</p>
       <div>
        <input type="checkbox" id="scales" name="scales">
        <label for="scales">{this.props.value[1]}</label>
        </input>
       </div>*/

       <div className ='this is it'>
        <input type="checkbox" id="horns" name="horns" onChange= {() =>this.props.onChange()}>
        <label for="horns">{this.props.value.text}</label>
        </input>
       </div>

      /*</div>*/

    );
  }
}


class Selection extends React.Component {
  constructor(props){

    super(props);
    this.state = {
      takers: Array(4).fill(null),
    };
  }
  /*state = {takers:Array(4).fill(null)};*/


  handleChange(st,i){
    const takers = this.state.takers.slice();
    takers[i]= 1;
    this.setState({takers:takers,});





  }
  renderTaker(st,i){
    return <Taker value = {{text:st, n:i}}
     onChange= {() => this.handleChange(st,i)} />;
  }

  render(){
    /*var option1 = ["Penalty taker", "Cristiano Ronaldo", "Lionel Messi"];
    var option2 = ["Freekick taker", "Lionel Messi", "Cristiano Ronaldo"];*/
    var question1 = "who will be the penalty taker?";
    var question2 = "who will be the freekick taker?";
    return(
      <div>
       <p>{question1}</p>
       <div className = 'selection1'>{this.renderTaker("Lionel Messi",0)}</div>

       <div className = 'selection2'>{this.renderTaker("Cristiano Ronaldo",1)}</div>

       <p>{question2}</p>
       <div className = 'selection3'>{this.renderTaker("Lionel Messi",2)}</div>

       <div className = 'selection4'>{this.renderTaker("Cristiano Ronaldo",3)}</div>



      </div>
    );
  }
}


class Game extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <Selection />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}





ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
/*serviceWorker.unregister();*/
